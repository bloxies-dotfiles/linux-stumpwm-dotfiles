;; -*-lisp-*-
;;
;; Here is a sample .stumpwmrc file

(in-package :stumpwm)

;; change the prefix key to something else

;; A bit hacky, setting the chord binding to super-F20 and then mapping super key with xcape to Super-F20
(set-prefix-key (kbd "s-F12"))

;; We will set the xcape in the xprofile file

(set-module-dir
 (pathname-as-directory (concat (getenv "HOME") "/.stumpwm.d/stumpwm-contrib")))

;; Custom modified useless gaps (fixing modeline issue)
(add-to-load-path "~/.stumpwm.d/modules-custom/spark/spark-gaps/")

;; GAPS CAN SOMETIMES BE BROKEN IF EMACS IS MANAGING YOUR WINDOWS
;; (load-module "spark-gaps")

;; ;; Head gaps run along the 4 borders of the monitor(s)
;; (setf spark-gaps:*head-gaps-size* 0)

;; ;; Inner gaps run along all the 4 borders of a window
;; (setf spark-gaps:*inner-gaps-size* 5)

;; ;; Outer gaps add more padding to the outermost borders of a window (touching
;; ;; the screen border)
;; (setf spark-gaps:*outer-gaps-size* 5)

;; ;; Call command
;; (spark-gaps:toggle-gaps-on)

;; TTF Font Support
;; (setf *window-border-style* :none)
;; (load-module "ttf-fonts")
;; (clx-truetype:cache-fonts)
;; (set-font (make-instance 'xft:font
;; 	   :family "Fira Mono"
;; 	   :subfamily "Regular"
;; 	   :size 9
;; 	   :antialias t))
;; Message window font
;; (set-font "-xos4-terminus-medium-r-normal--14-140-72-72-c-80-iso8859-15")
(set-font "-windows-dina-medium-r-normal--10-80-96-96-c-70-iso8859-1")

(setf *message-window-gravity* :center
      *input-window-gravity* :center
      spark/*which-key-window-gravity* :bottom-left
      *window-border-style* :thin
      *message-window-padding* 10
      *maxsize-border-width* 5
      *normal-border-width* 5
      *transient-border-width* 2
      stumpwm::*float-window-border* 2
      stumpwm::*float-window-title-height* 5
      *mouse-focus-policy* :click)

(set-normal-gravity :center)
(set-maxsize-gravity :center)
(set-transient-gravity :center)

(set-fg-color "#eeeeee")
(set-bg-color "#1C2028")
(set-border-color "#232731")
(set-focus-color "#3B4252")
(set-unfocus-color "#232731")
(set-win-bg-color "#22272F")
(set-float-focus-color "#3B4252")
(set-float-unfocus-color "#232731")

;;; Groups

;; First Group (Default)
(grename "Work")

;; Other Groups
(gnewbg "Browse")
(gnewbg "Chat")
(gnewbg "Game")

;;; Define window placement policy...

;; Clear rules
(clear-window-placement-rules)

;; Last rule to match takes precedence!
;; TIP: if the argument to :title or :role begins with an ellipsis, a substring
;; match is performed.
;; TIP: if the :create flag is set then a missing group will be created and
;; restored from *data-dir*/create file.
;; TIP: if the :restore flag is set then group dump is restored even for an
;; existing group using *data-dir*/restore file.
(define-frame-preference "Default"
  ;; frame raise lock (lock AND raise == jumpto)
  (0 t nil :class "Konqueror" :role "...konqueror-mainwindow")
  (1 t nil :class "XTerm"))

(define-frame-preference "Ardour"
  (0 t   t   :instance "ardour_editor" :type :normal)
  (0 t   t   :title "Ardour - Session Control")
  (0 nil nil :class "XTerm")
  (1 t   nil :type :normal)
  (1 t   t   :instance "ardour_mixer")
  (2 t   t   :instance "jvmetro")
  (1 t   t   :instance "qjackctl")
  (3 t   t   :instance "qjackctl" :role "qjackctlMainForm"))

(define-frame-preference "Shareland"
  (0 t   nil :class "XTerm")
  (1 nil t   :class "aMule"))

(define-frame-preference "Work"
  (0 t t :class "Emacs"))

(define-frame-preference "Browse"
  (0 t t :class "freetube")
  (0 t t :class "FreeTube")
  (0 t t :class "qutebrowser")
  (0 t t :class "chromium")
  (0 t t :class "Chromium"))

(define-frame-preference "Chat"
  (0 t t :class "discord"))

(define-frame-preference "Game"
  (0 t t :class "steam")
  (0 t t :class "Steam"))

;; (define-frame-preference "Emacs"
;;  (1 t t :restore "emacs-editing-dump" :title "...xdvi")
;;  (0 t t :create "emacs-dump" :class "Emacs"))

;;; StumpWM Spark's Custom Which Key Mode

(setf spark/*original-message-window-gravity* *message-window-gravity*)

;; Where message popups appear
(defun spark/display-bindings-for-keymaps (key-seq &rest keymaps)
  (let* ((screen (current-screen))
         (data (mapcan (lambda (map)
                         (mapcar (lambda (b)
                                   (let ((bound-to (binding-command b)))
                                     (format nil "^5*~5a^n ~a"
					     (print-key (binding-key b))
					     
                                             (cond ((or (symbolp bound-to)
                                                        (stringp bound-to))
                                                    bound-to)
                                                   ((kmap-p bound-to)
                                                    "Anonymous Keymap")
                                                   (t "Unknown"))
					     )))
                                 (kmap-bindings map)))
                       keymaps))
         (cols (ceiling (1+ (length data))
                        (truncate (- (head-height (current-head)) (* 2 (screen-msg-border-width screen)))
                                  (font-height (screen-font screen))))))
    
    ;; Kinda hacky implementation to set the position of the keymap window
    (setf *message-window-gravity* spark/*which-key-window-gravity*)
    
    (message-no-timeout "~{~a~^~%~%~}~%Prefix: ~a~%"

			;; The number of available columns is arbitrarily set, however, it would be good to calculate how many columns are allowed to be displayed and show accordingly
                        (or (columnize data 9) '("(EMPTY MAP)"))
			(print-key-seq key-seq)
			
			)
    ;; Change back the message location to specified
    (setf *message-window-gravity* spark/*original-message-window-gravity*)
    
    ))

(defun spark/which-key-mode-key-press-hook (key key-seq cmd)
  "*key-press-hook* for which-key-mode"
  (declare (ignore key cmd))
  (when (not (eq *top-map* *resize-map*))
    (let* ((oriented-key-seq (reverse key-seq))
           (maps (get-kmaps-at-key-seq (dereference-kmaps (top-maps)) oriented-key-seq)))
      (when-let ((only-maps (remove-if-not 'kmap-p maps)))
        (apply 'spark/display-bindings-for-keymaps oriented-key-seq only-maps)))))

(defcommand spark/which-key-mode () ()
  "Toggle which-key-mode"
  (if (find 'spark/which-key-mode-key-press-hook *key-press-hook*)
      (remove-hook *key-press-hook* 'spark/which-key-mode-key-press-hook)
      (add-hook *key-press-hook* 'spark/which-key-mode-key-press-hook)))


(spark/which-key-mode)

;;; StumpWM Spark's Custom Buffer Switcher

;; TODO

;; Make a new attribute that keeps track of popularity (how many times a window has been switched back to)

;; Are slots seriously only strings?

;; (define-window-slot sparkpopularity)
;; (setf sparkpopularity 1)

;; (defcommand spark/popularity-handler (to-window from-window) ()
;; "Toggle the current window's mark."
;;   (let ((win (current-window)))
;;     (when win
;;       ;; If the window-sparkpopularity is not equal to true (therefore false/nil), set it to a starting value
;;       (if (not (window-sparkpopularity win)) (setf (window-sparkpopularity win) (princ-to-string sparkpopularity)) (setf (window-sparkpopularity win) (princ-to-string (+ (parse-integer (window-sparkpopularity win)) 1))))
;;       ;; (message (window-sparkpopularity win))
;;       ))
;;   )

;; (stumpwm:add-hook stumpwm:*focus-window-hook* 'spark/popularity-handler)

;; (defmethod spark/sort-windows-by-popularity ((window-list list))
;;   "Return a copy of the provided window list sorted by class then by popularity."
;;   (sort1 window-list (lambda (w1 w2)
;;                        (let ((class1 (window-class w1))
;;                              (class2 (window-class w2)))
;;                          (if (string= class1 class2)
;;                            (< (parse-integer (window-sparkpopularity w2)) (parse-integer (window-sparkpopularity w1))
;;                            (string< class1 class2)))))))


;; It would make more sense that the window-list argument was before the fmt one
;; but window-list was added latter and I didn't want to break other's code.

;; (defcommand spark/windowlist-by-popularity (&optional (fmt *window-format*)
;;                                   window-list) (:rest)
;;   "Allow the user to select a window from the list of windows and focus the
;; selected window. For information of menu bindings see @ref{Menus}. The optional
;;  argument @var{fmt} can be specified to override the default window formatting.
;; The optional argument @var{window-list} can be provided to show a custom window
;; list (see @command{windowlist-by-class}). The default window list is the list of
;; all window in the current group. Also note that the default window list is sorted
;; by number and if the @var{windows-list} is provided, it is shown unsorted (as-is)."
;;   Shadowing the window-list argument.
;;   (if-let ((window-list (or window-list
;;                           (spark/sort-windows-by-popularity
;;                            (group-windows (current-group))))))
;;     (if-let ((window (select-window-from-menu window-list fmt)))
;;       (group-focus-window (current-group) window)
;;       (throw 'error :abort))
;;     (message "No Managed Windows"))

;;   )

;;; Keybindings

;; Exit stumpwm
(define-key *top-map* (kbd "M-q") "quit")

;; Restart StumpWM - Bugged for me
(define-key *top-map* (kbd "M-r") "restart-hard")

;; Rofi launcher
(define-key *root-map* (kbd "SPC") "run-shell-command rofi -show drun")

;; Workspace (Group) navigation
(define-key *top-map* (kbd "s-h") "gprev")
(define-key *top-map* (kbd "s-l") "gnext")
(define-key *groups-map* (kbd "b") "grouplist")

;; Managing windows & frames
(define-key *root-map* (kbd "0") "remove")
(define-key *root-map* (kbd "1") "only")
(define-key *root-map* (kbd "2") "vsplit")
(define-key *root-map* (kbd "3") "hsplit")
(define-key *root-map* (kbd "w") "remove")
(define-key *root-map* (kbd "W") "delete")

;; Managing layouts


;; Fuzzy finding buffers, like EMACS
;; frame-windowlist
(define-key *root-map* (kbd "b")   "windowlist-by-class")

;; We're also going to assign the window & frame management to the top map, meaning that we can hold down the super key without releasing the superkey and press these bindings simultaneously

(define-key *top-map* (kbd "s-SPC") "run-shell-command rofi -show drun")

(define-key *top-map* (kbd "s-b")   "windowlist-by-class")

(define-key *top-map* (kbd "s-h") "gprev")
(define-key *top-map* (kbd "s-l") "gnext")

(define-key *top-map* (kbd "s-0") "remove")
(define-key *top-map* (kbd "s-1") "only")
(define-key *top-map* (kbd "s-2") "vsplit")
(define-key *top-map* (kbd "s-3") "hsplit")
(define-key *top-map* (kbd "s-w") "remove")
(define-key *top-map* (kbd "s-W") "delete")
(define-key *top-map* (kbd "s-o") "fnext")

;; (define-key *root-map* (kbd "q")   "delete")
;; (define-key *root-map* (kbd "Q")   "kill-window")

;; (define-key *root-map* "s-f" "fullscreen")

;;; External Stumpwm stuff


;;; Autostart

(run-shell-command "~/.stumpwm.d/autostart.sh")
;; (run-shell-command "emacs")

;;; Random Example Junk

;; prompt the user for an interactive command. The first arg is an
;; optional initial contents.
;; (defcommand colon1 (&optional (initial "")) (:rest)
;;   (let ((cmd (read-one-line (current-screen) ": " :initial-input initial)))
;;     (when cmd
;;       (eval-command cmd t))))

;; Web jump (works for DuckDuckGo and Imdb)
;; (defmacro make-web-jump (name prefix)
;;   `(defcommand ,(intern name) (search) ((:rest ,(concatenate 'string name " search: ")))
;;     (nsubstitute #\+ #\Space search)
;;     (run-shell-command (concatenate 'string ,prefix search))))

;; (make-web-jump "duckduckgo" "firefox https://duckduckgo.com/?q=")
;; (make-web-jump "imdb" "firefox http://www.imdb.com/find?q=")

;; ;; C-t M-s is a terrble binding, but you get the idea.
;; (define-key *root-map* (kbd "M-s") "duckduckgo")
;; (define-key *root-map* (kbd "i") "imdb")
