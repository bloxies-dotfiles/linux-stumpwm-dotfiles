#!/usr/bin/env sh

## Add this to your wm startup file.

# Terminate already running bar instances
killall -q polybar
killall -q dunst
killall -q nm-applet
killall -q udiskie
killall -q polkit-gnome-authentication-agent-1

wpg -s ~/.stumpwm.d/wallpapers/
# wal -i ~/.stumpwm.d/wallpapers/

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch bar
polybar -c $HOME/.stumpwm.d/polybar/config -r main &
dunst &
nm-applet &
udiskie &
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1
