;;;; swm-calibre.asd

(asdf:defsystem #:spark-gaps
  :description "Pretty (useless) gaps for StumpWM, modified by spark to fix bugs."
  :author "vlnx <https://github.com/vlnx>, Abhinav Tushar <abhinav.tushar.vs@gmail.com>"
  :depends-on (#:stumpwm)
  :serial t
  :components ((:file "package")
               (:file "spark-gaps")))
